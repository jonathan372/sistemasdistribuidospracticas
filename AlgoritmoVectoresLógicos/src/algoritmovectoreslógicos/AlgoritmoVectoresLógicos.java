/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmovectoreslógicos;

/**
 *
 * @author Jonathan Javier
 */
public class AlgoritmoVectoresLógicos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String p1[] = {"a", "b"};
        String p2[] = {"c", "d"};
        String p3[] = {"e", "f"};
        
        int valoresP1[][] = {{0, 0, 0}, {0, 0, 0}};
        int valoresP2[][] = {{0, 0, 0}, {0, 0, 0}};
        int valoresP3[][] = {{0, 0, 0}, {0, 0, 0}};
        
        int valorInicialP1 = 0;
        int valorInicialP2 = 0;
        int valorInicialP3 = 0;
        
        //Existe unión entre "b" y "c"
        //Existe unión entre "d" y "f"
        
        if(valorInicialP1 == 0 && valoresP1[0][0] == 0){
            valorInicialP1 = 1;
            valoresP1[0][0] = 1;
        }
        
        if (valoresP1[0][0] > 0) {
            valoresP1[1][0] = valoresP1[0][0]+1;
        }
        
        if(valoresP1[0][0] > valoresP2[0][0]){
            valoresP2[0][0] = valoresP1[1][0];
        }
        
        if(valoresP1[1][1] > valorInicialP2){
            valorInicialP2 = 1;
            valoresP2[0][1] = valoresP1[1][1]+1;
        }else{
            valoresP2[0][1] = valorInicialP2+1;
        }
        
        if (valoresP2[0][0]>valorInicialP2) {
            valoresP2[1][0]=valoresP2[0][0];
            valoresP2[1][1]=valoresP2[0][1]+1;
        }
        
        
        if (valorInicialP3 == 0 && valoresP3[0][2] == 0) {
            valorInicialP3 = 1;
            valoresP3[0][2] = 1;
        }
        
        if(valoresP2[1][2]>valorInicialP3){
            valoresP3[1][2] = valoresP2[1][2]+1; 
        }else{
            valoresP3[1][2] = valorInicialP3+1;
        }
        if(valoresP2[1][0]>valoresP3[1][0]){
            valoresP3[1][0] = valoresP2[1][0]; 
        }
        if(valoresP2[1][1]>valoresP3[1][1]){
            valoresP3[1][1] = valoresP2[1][1]; 
        }
        
        System.out.print("P1---");
        for (int i = 0; i < p1.length; i++) {
            System.out.print(p1[i]+"=[");
            for (int j = 0; j < 3; j++) {
                System.out.print(valoresP1[i][j]);
                if(j<2){
                    System.out.print(",");
                }
            }
            System.out.print("]-----");
        }
        System.out.println("\n");
        
        System.out.print("P2---");
        for (int i = 0; i < p2.length; i++) {
            System.out.print(p2[i]+"=[");
            for (int j = 0; j < 3; j++) {
                System.out.print(valoresP2[i][j]);
                if(j<2){
                    System.out.print(",");
                }
            }
            System.out.print("]-----");
        }
        System.out.println("\n");
        
        System.out.print("P3---");
        for (int i = 0; i < p3.length; i++) {
            System.out.print(p3[i]+"=[");
            for (int j = 0; j < 3; j++) {
                System.out.print(valoresP3[i][j]);
                if(j<2){
                    System.out.print(",");
                }
            }
            System.out.print("]-----");
        }
        System.out.println("\n");
    }
    
}
