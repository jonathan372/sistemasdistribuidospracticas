import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

public class ServidorMulti{
    private static ServerSocket serverSocket;
    private static Socket cliente;

    public static void main(String[] args) throws SocketException, IOException{
        try {
            serverSocket = new ServerSocket(9876);
        } catch (Exception e) {
            e.getMessage();
        }

        while (true){
            cliente = serverSocket.accept();
            Thread hilo = new Thread(){
                public void run(){
                    try (Scanner scanner = new Scanner(cliente.getInputStream());){
                        while(scanner.hasNextLine()){
                            String mensaje = scanner.nextLine();
                            System.out.println("SE HA RECIBIDO EL MENSAJE: " + mensaje);
                        }    
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
            };
            hilo.start();
        }
    }
}