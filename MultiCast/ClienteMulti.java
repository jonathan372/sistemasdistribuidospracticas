import java.io.IOException;
import java.io.PrintWriter;
import java.net.*;
import java.util.Scanner;

public class ClienteMulti {
    private static int puerto = 9876;
    private static String host = "127.0.0.1";
    private static Socket socketCliente;
    public static void main(String[] args) throws SocketException, IOException, InterruptedException {
        socketCliente = new Socket(host, puerto);
        int contador = 1;

        try (
            PrintWriter envio = new PrintWriter(socketCliente.getOutputStream(), true);
            Scanner scanner = new Scanner(socketCliente.getInputStream());
            Scanner msj = new Scanner(System.in);
        ){
            while(true){
                // System.out.println("Enviar dato al servidor");
                envio.println("Mensaje número " + contador);
                System.out.println("Envía mensaje número " + contador);
                contador++;
                Thread.sleep(1000);
                
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }
}