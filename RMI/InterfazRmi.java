import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfazRmi extends Remote{
    public String saludar(String msj) throws RemoteException;
}