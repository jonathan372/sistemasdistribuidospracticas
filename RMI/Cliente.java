import java.rmi.Naming;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Cliente {
    public static void main(String[] args) {
        try {
            InterfazRmi interfaz = (InterfazRmi)Naming.lookup("rmi://localhost/saludo");
            System.out.println("Como te llamas");
            Scanner scan = new Scanner(new InputStreamReader(System.in));
            System.out.println(interfaz.saludar(scan.next()));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}