/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "FuncionesLogin.h"
#include "string.h"
char **
msj_1_svc(mensaje *argp, struct svc_req *rqstp)
{
	static char * result;
	char usuarios[][20] = {"Jonathan", "Javier", "Areli", "Andres", "Admin"};
	char claves[][20] = {"jona123", "javi123", "are123", "andres123", "admin"};

	char datosCorrectos[] = "Se ha logeado con éxito, BIENVENIDO";
	char usuarioIncorrecto[] = "El nombre de usuario no existe";
	char claveIncorrecta[] = "La clave ingresa es incorrecta";
	char charUsuario[20];
	char charClave[20];
	sprintf(charUsuario, "%s", argp->usuario);
	sprintf(charClave, "%s", argp->clave);
	int longitudDelArreglo = sizeof(usuarios) / sizeof(usuarios[0]);
	for(int i = 0; i < longitudDelArreglo; i++){
		if(strcmp(charUsuario, usuarios[i])==0){
			printf("Comprobar");
			if(strcmp(charClave, claves[i])==0){
				result = strdup(datosCorrectos);
			}else{
				result = strdup(claveIncorrecta);
			}
		}else{
			printf("Comprobar");
			result = strdup(usuarioIncorrecto);
		}
	}

	/*
	 * insert server code here
	 */

	return &result;
}
