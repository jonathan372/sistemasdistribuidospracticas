/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicasistemasdistribuidos1.udp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 *
 * @author Jonathan Javier
 */
public class UdpCliente {
    public static void main (String [] args) throws SocketException, IOException{
        DatagramSocket clientSocket = new DatagramSocket();
        BufferedReader inFromUser =  new BufferedReader(new InputStreamReader(System.in));
        
        InetAddress IPAddress = InetAddress.getByName("127.0.0.1");
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];
        
        System.out.println("Envio un dato al servidor");
        String sentence = inFromUser.readLine();
        sendData = sentence.getBytes();
        
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 9876);
        clientSocket.send(sendPacket);
        System.out.println("MESSAGE SENT");
        
        //Recibir mensaje
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocket.receive(receivePacket);
        String modifiedSentence =  new String(receivePacket.getData());
        System.out.println("FORM SERVER: " + modifiedSentence);
        clientSocket.close();
        
    }
}
