/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicasistemasdistribuidos1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Jonathan Javier
 */
public class Servidor {
    
    private static ServerSocket server;
    private static int port = 5000;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // TODO code application logic here
        server = new ServerSocket(port);
        while(true){
            System.out.println("Waiting for client request");
            Socket socket = server.accept();
            ObjectInputStream ois =  new ObjectInputStream(socket.getInputStream());
            String message = (String)ois.readObject();
            ois.close();
            socket.close();
            System.out.println("Mensaje recibido: " + message);
            System.out.println("Finishing the request\n");
        }
        //server.close();
    }
    
}
