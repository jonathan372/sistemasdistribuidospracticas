#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void *factorial(void *n_void_ptr);
int suma = 0;

int main(){
    
    void *status;
    for (int i = 1; i <= 10; i++){
        pthread_t factorial_thread;
        pthread_create(&factorial_thread, NULL, &factorial, &i);
        pthread_join(factorial_thread, NULL);
    }
    printf("La suma de los factoriales es: %d \n", suma);
}

void *factorial(void *n_void_ptr){
    int c;    
    int *x_ptr = (int *)n_void_ptr;
    int resultado = 1;

    for (c = 1; c <= *x_ptr; c++){
        resultado = resultado * c;
    }
    printf("Valor de Factorial (%d): %d \n", *x_ptr, resultado);
    suma = suma + resultado;
    return NULL;
}
