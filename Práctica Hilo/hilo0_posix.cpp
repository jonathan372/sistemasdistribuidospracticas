#include <pthread.h>
#include <stdio.h>
#define MAX 100


int factorial(int n);

int main(){
	int x = 0;

    for (int i = 3; i <= 10; i++){
        x = factorial(i);
        printf("Valor de Factorial (%d): %d \n", i, x);
    }
    
    
}

int factorial(int n){
    int c;
    int resultado = 1;

    for (c = 1; c <= n; c++){
        resultado = resultado * c;
    }

    return resultado;
}
