/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmolamport;

import java.util.Arrays;

/**
 *
 * @author Jonathan Javier
 */
public class AlgoritmoLamport {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String p1[] = {"a", "b"};
        String p2[] = {"c", "d"};
        String p3[] = {"e", "f"};
        
        int valoresP1[] = {0, 0};
        int valoresP2[] = {0, 0};
        int valoresP3[] = {0, 0};
        
        int valorInicialP1 = 0;
        int valorInicialP2 = 0;
        int valorInicialP3 = 0;
        
        //Existe unión entre "b" y "c"
        //Existe unión entre "d" y "f"
        
        if(valorInicialP1 == 0 && valoresP1[0] == 0){
            valorInicialP1 = 1;
            valoresP1[0] = 1;
        }
        
        if (valoresP1[0] > 0) {
            valoresP1[1] = valoresP1[0]+1;
        }
        
        if (valorInicialP2 < valoresP1[1]) {
            valorInicialP2 = valoresP1[1]+1;
            valoresP2[0] = valorInicialP2;
        }else{
            valorInicialP2 = 1;
            valoresP2[0] = 1;
        }
        
        if (valoresP2[0] > 0) {
            valoresP2[1] = valoresP2[0]+1;
        }
        
        if(valorInicialP3 == 0 && valoresP3[0] == 0){
            valorInicialP3 = 1;
            valoresP3[0] = 1;
        }
        
        if (valorInicialP3 < valoresP2[1]) {
            valorInicialP3 = valoresP2[1]+1;
            valoresP3[1] = valorInicialP3;
        }else{
            valoresP3[1] = valoresP3[0]+1;
        }
        
        System.out.print("P1---");
        for (int i = 0; i < p1.length; i++) {
            System.out.print(p1[i]+"="+valoresP1[i]+"------");
        }
        System.out.println("\n");
        System.out.print("P2---");
        for (int i = 0; i < p2.length; i++) {
            System.out.print(p2[i]+"="+valoresP2[i]+"------");
        }
        System.out.println("\n");
        System.out.print("P1---");
        for (int i = 0; i < p3.length; i++) {
            System.out.print(p3[i]+"="+valoresP3[i]+"------");
        }
        System.out.println("\n");
        
        System.out.print("Procesos enlistados: ");
        int lista[]={0, 0, 0, 0, 0, 0};
        
        for (int i = 0; i < p1.length; i++) {
            String resp = String.valueOf(valoresP1[i])+String.valueOf(1); 
            lista[i] = Integer.parseInt(resp);
        }
        for (int i = 0; i < p2.length; i++) {
            String resp = String.valueOf(valoresP2[i])+String.valueOf(2);
            lista[i+2] = Integer.parseInt(resp);
        }
        for (int i = 0; i < p3.length; i++) {
            String resp = String.valueOf(valoresP3[i])+String.valueOf(3);
            lista[i+4] = Integer.parseInt(resp);
        }
        
        for (int i = 0; i < lista.length; i++) {
            System.out.print("\t" + lista[i]);
        }
        System.out.println("\n");
        
        System.out.print("Procesos ordenados: ");
        Arrays.sort(lista);
        
        for (int i = 0; i < lista.length; i++) {
            System.out.print("\t" + lista[i]);
        }
        System.out.println("\n");
    }
}
