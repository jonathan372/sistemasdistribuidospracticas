import java.io.*;
import java.rmi.*;
import java.util.Scanner;

public class Cliente {
    public static void main(String[] args) {
        try {
            InterfazRmi interfaz = (InterfazRmi) Naming.lookup("rmi://localhost/banco");
            Boolean continuar =  true;
            while (continuar) {
                System.out.println("Ingrese el numero de la acción a realizar:");
                System.out.println("1. Depositar");
                System.out.println("2. Retirar");
                System.out.println("3. Salir");
                Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
                int opcion = scanner1.nextInt();
                Scanner scanner2;
                Double valor;
                if (opcion == 1) {
                    System.out.print("\nIngrese el valor que desea depositar: ");
                    scanner2 = new Scanner(new InputStreamReader(System.in));
                    valor = scanner2.nextDouble();
                    System.out.println(interfaz.depositar(valor));
                }else if (opcion == 2) {
                    System.out.print("\nIngrese el valor que desea retirar: ");
                    scanner2 = new Scanner(new InputStreamReader(System.in));
                    valor = scanner2.nextDouble();
                    System.out.println(interfaz.retirar(valor));
                }else if (opcion == 3) {
                    System.out.println("GRACIAS POR VISITAR NUESTRO BANCO\n");
                    continuar = false;
                }else if (opcion < 0 || opcion > 3) {
                    System.out.println("La opción ingresada no existe\n");
                }

                System.out.println("-----------------------------------------------------------------------\n");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}