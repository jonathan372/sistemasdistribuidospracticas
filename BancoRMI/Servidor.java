import java.rmi.*;

public class Servidor {
    public Servidor(){
        try{
            InterfazRmi objetoRmi = new ImplementacionRmi();
            Naming.rebind("rmi://localhost/banco", objetoRmi);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
    public static void main(String[] args)  throws RemoteException{
        new Servidor();
    }
}
