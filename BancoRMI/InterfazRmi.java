import java.rmi.*;

public interface InterfazRmi extends Remote{
    public String depositar(Double cantidad) throws RemoteException;
    public String retirar(Double cantidad) throws RemoteException;
}