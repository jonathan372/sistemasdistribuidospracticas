import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;

public class ImplementacionRmi extends UnicastRemoteObject implements InterfazRmi{
    
    Double montoTotal = 0.0;

    public ImplementacionRmi() throws RemoteException{
        super();
    }  

    public String depositar(Double valorDeposito) throws RemoteException{
        montoTotal = montoTotal + valorDeposito;
        return "Ha realizado un deposito de $" + valorDeposito + "------> Saldo Actual: $" + montoTotal + "\n";
    }

    public String retirar(Double valorRetiro) throws RemoteException{
        String respuesta = "";
        if (valorRetiro<=montoTotal) {
            montoTotal = montoTotal - valorRetiro;
            respuesta = "Ha realizado un retiro de $" + valorRetiro + "------> Saldo Actual: $" + montoTotal + "\n";      
        }else if(valorRetiro>montoTotal){
            respuesta = "No tiene saldo suficiente para retirar";
        }
        return respuesta;
    }
}