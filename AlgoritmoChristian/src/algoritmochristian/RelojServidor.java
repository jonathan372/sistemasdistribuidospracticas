/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmochristian;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Jonathan Javier
 */
public class RelojServidor {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException{
        // TODO code application logic here
        int puerto = 30000;
        
        try (
                ServerSocket serverSocket = new ServerSocket(puerto);
                Socket clienteSocket = serverSocket.accept();
                PrintWriter salida = new PrintWriter(clienteSocket.getOutputStream(), true);
                BufferedReader entrada = new BufferedReader(new InputStreamReader(clienteSocket.getInputStream()));
            )
        {
            String inputLine;
            System.out.println("Servidor Corriendo");
            while (true) {                
                inputLine =  entrada.readLine();
                if (inputLine.equalsIgnoreCase("Salir")) {
                    System.out.println("Saliendo");
                    salida.println("Servidor cerrado");
                    break;
                }
                salida.println(System.currentTimeMillis()-6000);
            }
        } catch (Exception e) {
            System.out.println("Ha ocurrido un error");
        }
    }
}
