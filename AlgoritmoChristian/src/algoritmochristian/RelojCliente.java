/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmochristian;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Jonathan Javier
 */
public class RelojCliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        int puerto = 30000;
        String hostname = "localhost";
        try(
                Socket nuevoSocket = new Socket(hostname, puerto);
                PrintWriter salida = new PrintWriter(nuevoSocket.getOutputStream(), true);
                BufferedReader entrada =  new BufferedReader(new InputStreamReader(nuevoSocket.getInputStream()));
            ){
            String ingresoUsuario;
            System.out.println("Se inicia el cliente");
            
            long tiempoInicial;
            long tiempoServidor;
            long tiempoFinal;
            long christianFinal;
            
            salida.println(tiempoInicial=System.currentTimeMillis());
            tiempoServidor = Long.parseLong(entrada.readLine());
            
            tiempoFinal = System.currentTimeMillis();
            christianFinal = (tiempoServidor + (tiempoFinal-tiempoInicial)/2);
            DateFormat formato = new SimpleDateFormat("HH:mm:ss");
            
            System.out.println("Tiempo del cliente: " + formato.format(new Date(tiempoFinal)));
            System.out.println("Tiempo del servidor: " + formato.format(new Date(tiempoServidor)));
            System.out.println("Tiempo del cliente actualizado: " + formato.format(new Date(christianFinal)));
            salida.println("Cerrando cliente");
        } catch (Exception e) {
            System.out.println("Ocurrió un error");
        }
    }
    
}
