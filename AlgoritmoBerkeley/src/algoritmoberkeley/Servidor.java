/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmoberkeley;

/**
 *
 * @author Jonathan Javier
 */
public class Servidor extends Thread{
    private MonitorSimulacion sm;
    private final int sleepMSeconds;
    private long serverTime;
    
    public Servidor(MonitorSimulacion sm){
        this.sm = sm;
        this.sleepMSeconds = 5000; // 5s
        this.serverTime = System.nanoTime();
    }
    
    public void run(){
        while(true){
            try{
                Thread.sleep(this.sleepMSeconds); // Duerme 10s
                // Despierta //
                /** Configura la hora del servidor en SimulatorMonitor, avisando a los clientes
                 * de que ya está configurada, el servidor se pondrá a dormir **/
                System.out.println("Temporización (servidor) : " + this.serverTime);
                this.sm.setServerTime(this.serverTime);
                /** Una vez despierte, los clientes ya habrán configurado las diferencias en el array
                 * se debe calcular la media y configurar los ajustes **/
                this.sm.calcAvgAndSet();
                /** Ajustar la hora del servidor (horaServidor + media) **/
                this.serverTime += this.sm.getAverage();
                /** Imprimir la temporización acordada **/
                System.out.println("Temporización acordada (servidor) : " + this.serverTime);           
                /** Cuando el servidor finalize se debe restaurar el estado inicial **/
                this.sm.restartProcess();
            }catch(InterruptedException e){} 
        }
    }
}
