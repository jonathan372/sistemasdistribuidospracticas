/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmoberkeley;

/**
 *
 * @author Jonathan Javier
 */
public class Simulacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        MonitorSimulacion sm = new MonitorSimulacion();
        // Crear un hilo servidor e iniciarlo //
        Servidor srv = new Servidor(sm);
        srv.start();
        Cliente clv[] = new Cliente[3];
        // Crear hilos clientes e iniciarlos //
        for(int i=0;i<3;i++){
            clv[i] = new Cliente(i,sm);
            clv[i].start();
        }
    }
    
}
