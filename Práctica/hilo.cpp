#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 100
#define MAX_THREADS 1
#define VECTOR_SIZE 12500000

void *inc_x(void *x_void_ptr);

int main(){
	int x = 0;
	int y = 0;
    void *status;
    clock_t t1, t2;
	printf("x: %d, y: %d\n", x, y); // imprimir los valores de x y y
    //HILO
    pthread_t inc_x_thread;
    t1 = clock();
	pthread_create(&inc_x_thread, NULL, &inc_x, &x);
    //inc_x(x);
    t2 = clock();
	//JOIN
    // rc = pthread_join(inc_x_thread, status);
    // if(rc){
    //     printf("ERROR; return code from pthread() is %d \n", rc);
    //     exit(-1);
    // }else{
    //     printf("Thread [%ld] exited with status [%ld] \n", i, (long)status);
    // }
	while (++y <MAX);
	printf("y terminó el incremento\n");
	printf("X: %d, Y: %d\n", x, y);
    printf("Tiempo total: %f\n",(((float)t2 - (float)t1) /1000000.0F) * 1000);
	return 0;
}

void *inc_x(void *x_void_ptr){
	int *x_ptr = (int *)x_void_ptr;
	printf("Estoy en el método implementado\n");

	while (++(*x_ptr) <MAX);
	printf("x terminó el incremento\n");
	printf("x: %d \n", *x_ptr);
    return NULL;
}