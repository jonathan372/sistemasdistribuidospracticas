/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "Funciones.h"


void
funcionesbancorpc_1(char *host)
{
	CLIENT *clnt;
	char * *result_1;
	escoger  msj_1_arg;
	printf("Ingrese el número de la acción a realizar (Tiene un saldo inicial de $100)\n");
	printf("1. Depositar\n");
	printf("2. Retirar\n");
	scanf("%i", &msj_1_arg.opcion);
	printf("Ingrese la cantidad que desea depositar/retirar: ");
	scanf("%i", &msj_1_arg.valor);
	printf("\n");

#ifndef	DEBUG
	clnt = clnt_create (host, FUNCIONESBANCORPC, VERSION_MENSAJE, "udp");
	if (clnt == NULL) {
		clnt_pcreateerror (host);
		exit (1);
	}
#endif	/* DEBUG */

	result_1 = msj_1(&msj_1_arg, clnt);
	printf("INFORMACIÓN DE LA TRANSACCIÓN\n");
	printf("%s\n", *result_1);
	if (result_1 == (char **) NULL) {
		clnt_perror (clnt, "call failed");
	}
#ifndef	DEBUG
	clnt_destroy (clnt);
#endif	 /* DEBUG */
}


int
main (int argc, char *argv[])
{
	char *host;

	if (argc < 2) {
		printf ("usage: %s server_host\n", argv[0]);
		exit (1);
	}
	host = argv[1];
	funcionesbancorpc_1 (host);
exit (0);
}
